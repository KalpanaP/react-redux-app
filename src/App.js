import logo from './logo.svg';
import './App.css';
import { connect, useDispatch } from 'react-redux';
import { demoActionRequset } from './redux/actions/demoAction';

const mapStateToProps = state => ({
  demoData: state.demoData
})

function App() {
  const dispatch = useDispatch();
  const data = {
    apiId: '045411e4-7fa9-441c-83a2-33f0bf92a535'
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <button onClick={() => dispatch(demoActionRequset(data))}>Dispatch demoActionRequset</button>
      </header>
    </div>
  );
}

export default connect(mapStateToProps)(App);
