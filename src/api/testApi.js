import axios from 'axios';
// import { response } from 'express';

const axiosInstance = axios.create({
    baseURL: 'https://run.mocky.io'
})

//apiId = 045411e4-7fa9-441c-83a2-33f0bf92a535
export function getTestApi(apiId) {
    return axiosInstance.get(`/v3/${apiId}`).then((response) => response).catch((error) => error);
}