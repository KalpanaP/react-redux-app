import { call, takeLatest, put } from 'redux-saga/effects';
import {
    DEMO_ACTION_REQUEST,
    DEMO_ACTION_SUCCESS,
    DEMO_ACTION_ERROR
} from '../actions/types';
import { getTestApi } from '../../api/testApi';

const demoSaga = [
    takeLatest(DEMO_ACTION_REQUEST, workerSagaDemoActionRequest)
];

// export function* demoSaga() {
//     yield takeLatest(DEMO_ACTION_REQUEST, workerSagaDemoActionRequest);
// }

function* workerSagaDemoActionRequest(action) {
    const { data } = action;

    try {
        const response = yield call(getTestApi, data.apiId);
        console.log("response: ", response);
        if (response.status == 200) {
            yield put({ type: DEMO_ACTION_SUCCESS, payload: response.data });
        } else {
            throw response;
        }
    } catch(error) {
        console.log("error: ", error);
        yield put({ type: DEMO_ACTION_ERROR, payload: error.data });
    }
}

export default demoSaga;

