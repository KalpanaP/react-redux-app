import { DEMO_ACTION_REQUEST } from './types';

export const demoActionRequset = (data) => {
    return {
        type: DEMO_ACTION_REQUEST,
        data
    };
};
