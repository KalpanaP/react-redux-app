import { combineReducers } from "redux";
import demoReducer from "./demoReducer";

const appReducer = combineReducers({
    demoReducer
});

const rootReducer = (state, action) => {
    return appReducer(state, action);
}

export default rootReducer;