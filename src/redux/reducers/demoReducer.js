import {
    DEMO_ACTION_REQUEST,
    DEMO_ACTION_SUCCESS,
    DEMO_ACTION_ERROR
} from '../actions/types';

const initialState = {
    isLoading: false,
    error: '',
    responseData: {},
    isProcessed: false
}

export default (state = initialState, action) => {
    switch (action.type) {
      case DEMO_ACTION_REQUEST:
        return {
          ...state,
          isLoading: true,
          isProcessed: false,
        };
      case DEMO_ACTION_SUCCESS: {
        return {
          ...state,
          isLoading: false,
          isProcessed: true,
          responseData: action.payload,
        };
      }
      case DEMO_ACTION_ERROR:
        return {
          ...state,
          isLoading: false,
          isProcessed: false,
          error: action.error,
        };
      default:
        return state;
    }
  };